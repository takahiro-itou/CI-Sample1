#!/bin/bash  -xue

work_dir=$(pwd)

while read local_ref local_sha1 remote_ref remote_sha1
do
    echo "local_ref = ${local_ref}"     1>&2
    echo "local_sha1 = ${local_sha1}"   1>&2
    echo "remote_ref = ${remote_ref}"   1>&2
    echo "remote_sha1 = ${remote_sha1}" 1>&2

    cd "{work_dir}"

    if [[ ! -d .worktree-test ]] ; then
        git worktree add .worktree-test ${local_ref}
    fi

    cd .worktree-test

    git resotre .
    git checkout ${local_ref}

    ./bootstrap  \
        &&  ./configure  \
        &&  make  \
        &&  make test
    ret_code=$?

    if [[ ${ret_code} -ne 0 ]] ; then
        echo "[ERROR] branch ${local_ref} test failure."  1>&2
        exit  ${ret_code}
    fi
done
